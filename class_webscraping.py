import requests as rt
from bs4 import BeautifulSoup as bs

#import html5lib





class web_scrap:

    def __init__(self,URL):
        self.URL = URL
        
    def content(self):
        r = rt.get(url = self.URL)
        htmlcontent = r.content
        return htmlcontent


    def content_f(self):
        r = rt.get(url = self.URL)
        htmlcontent = r.content
        soup = bs(htmlcontent, 'html.parser')
        return soup.prettify


    def all_a_tag(self):
        r = rt.get(url = self.URL)
        htmlcontent = r.content
        soup = bs(htmlcontent, 'html.parser')
        anchor = soup.find_all('a')
        for i in anchor:
            print(i.get('href'))



    def all_p_tag(self):
        r = rt.get(url = self.URL)
        htmlcontent = r.content
        soup = bs(htmlcontent, 'html.parser')
        para = soup.find_all('p')
        for i in para:
    
            print(' # '*30) 
            
            print(i.get_text())



web = web_scrap("https://pypi.org/project/pandas/")
web.all_p_tag()


